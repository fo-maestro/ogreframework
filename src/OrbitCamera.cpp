#include "OgreFramework.h"

OGFW::OrbitCamera::OrbitCamera(Ogre::SceneManager* sceneManager, Ogre::String cameraName)
        : speed(70),
          sensitivyX(0.25f),
          sensitivyY(0.05f),
          cam(0),
          cam_node(0),
          mYaw(0),
          pitch_node(0),
          mPitch(0),
          zoomSpeed(0.05f)
{
    cam = sceneManager->createCamera(cameraName);
    cam_node = sceneManager->getRootSceneNode()->createChildSceneNode();
    pitch_node = cam_node->createChildSceneNode();
    pitch_node->attachObject(cam);
}

OGFW::OrbitCamera::~OrbitCamera() {}

void OGFW::initViewPort(Ogre::RenderWindow *window, Ogre::Camera *camera)
{
    Ogre::Viewport* vp = window->addViewport(camera);
    vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));
    camera->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
}

void OGFW::OrbitCamera::translate(Ogre::Vector3 direction)
{
    mYaw = Ogre::Degree(0);
    cam_node->translate(direction * speed, Ogre::Node::TS_LOCAL);
}

void OGFW::OrbitCamera::translate(Ogre::Vector3 direction, Time time)
{
    mYaw = Ogre::Degree(0);
    cam_node->translate(direction * speed * time.deltaTime, Ogre::Node::TS_LOCAL);
}

Ogre::Degree OGFW::OrbitCamera::getYaw()
{
    return mYaw;
}

Ogre::Degree OGFW::OrbitCamera::getPitch()
{
    return mPitch;
}

void OGFW::OrbitCamera::rotate(Ogre::Real x, Ogre::Real y)
{
    mYaw += Ogre::Degree(x * sensitivyX);
    mPitch += Ogre::Degree(y * sensitivyY);
    cam_node->yaw(Ogre::Degree(x * sensitivyX));
    pitch_node->pitch(Ogre::Degree(y * sensitivyY));
}

void OGFW::OrbitCamera::rotate(Ogre::Real x, Ogre::Real y, Time time)
{
    mYaw += Ogre::Degree(x * sensitivyX * time.deltaTime);
    mPitch += Ogre::Degree(y * sensitivyY);
    cam_node->yaw(Ogre::Degree(x * sensitivyX * time.deltaTime));
    pitch_node->pitch(Ogre::Degree(y * sensitivyY * time.deltaTime));
}

void OGFW::OrbitCamera::setSensitivy(Ogre::Real sensitivyX, Ogre::Real sensitivyY)
{
    this->sensitivyX = sensitivyX;
    this->sensitivyY = sensitivyY;
}

void OGFW::OrbitCamera::setSpeed(Ogre::Real speed)
{
    this->speed = speed;
}

void OGFW::OrbitCamera::setTargetPosition(Ogre::Vector3 position)
{
    cam_node->setPosition(position);
}

void OGFW::OrbitCamera::setNearClipDistance(Ogre::Real distance)
{
    cam->setNearClipDistance(distance);
}

Ogre::Camera* OGFW::OrbitCamera::getCamera()
{
    return cam;
}

Ogre::SceneNode* OGFW::OrbitCamera::getTargetNode()
{
    return cam_node;
}

Ogre::Real OGFW::OrbitCamera::getSpeed()
{
    return speed;
}

Ogre::Real OGFW::OrbitCamera::getSensitivyX()
{
    return sensitivyX;
}

Ogre::Real OGFW::OrbitCamera::getSensitivyY()
{
    return sensitivyY;
}

Ogre::Vector3 OGFW::OrbitCamera::getCameraPosition()
{
    return cam->getPosition();
}

Ogre::Quaternion OGFW::OrbitCamera::getCameraOrientation()
{
    return cam->getOrientation();
}

Ogre::Quaternion OGFW::OrbitCamera::getTargetOrientation()
{
    return cam_node->getOrientation();
}

void OGFW::OrbitCamera::setTargetOrientation(Ogre::Quaternion orientation)
{
    cam_node->setOrientation(orientation);
}

Ogre::Vector3 OGFW::OrbitCamera::getTargetPosition()
{
    return cam_node->getPosition();
}

Ogre::Real OGFW::OrbitCamera::getZoomSpeed()
{
    return zoomSpeed;
}

void OGFW::OrbitCamera::setCameraElevation(Ogre::Real elevation)
{
    cam_node->translate(Ogre::Vector3(0, elevation, 0), Ogre::Node::TS_LOCAL);
}

void OGFW::OrbitCamera::setCameraDistance(Ogre::Real distance)
{
    cam->moveRelative(Ogre::Vector3(0, 0, distance));
}

void OGFW::OrbitCamera::setZoomSpeed(Ogre::Real speed)
{
    zoomSpeed = speed;
}

void OGFW::OrbitCamera::yaw(Ogre::Degree degree)
{
    pitch_node->yaw(degree);
}

void OGFW::OrbitCamera::pitch(Ogre::Degree pitch)
{
    pitch_node->pitch(pitch);
}

void OGFW::OrbitCamera::zoom(Ogre::Real zoom)
{
    cam->moveRelative(Ogre::Vector3(0, 0, -zoom) * zoomSpeed);
}

void OGFW::OrbitCamera::zoom(Ogre::Real zoom, Time time)
{
    cam->moveRelative(Ogre::Vector3(0, 0, -zoom) * zoomSpeed * time.deltaTime);
}