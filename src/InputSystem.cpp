#include "OgreFramework.h"

OGFW::InputSystem::InputSystem(Ogre::RenderWindow* window, float* kAXISVertical, float* kAXISHorizontal, float* kWSVertical, float *kADHorizontal)
        : kAXISVertical(kAXISVertical),
          kAXISHorizontal(kAXISHorizontal),
          kWSVertical(kWSVertical),
          kADHorizontal(kADHorizontal)
{
    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    window->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

    inputManager = OIS::InputManager::createInputSystem(pl);
    keyboard = static_cast<OIS::Keyboard*>(inputManager->createInputObject(OIS::OISKeyboard, true));
    mouse = static_cast<OIS::Mouse*>(inputManager->createInputObject(OIS::OISMouse, true));
}

OGFW::InputSystem::~InputSystem()
{
    inputManager->destroyInputObject(mouse);
    inputManager->destroyInputObject(keyboard);
    OIS::InputManager::destroyInputSystem(inputManager);
    inputManager = 0;
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Destroy Inputs ***");
}

bool OGFW::InputSystem::isRunning()
{
    return inputManager != 0;
}

void OGFW::InputSystem::setKeyListener(OIS::KeyListener *listener)
{
    keyboard->setEventCallback(listener);
}

void OGFW::InputSystem::setMouseListener(OIS::MouseListener *listener)
{
    mouse->setEventCallback(listener);
}

void OGFW::InputSystem::capture()
{
    keyboard->capture();
    mouse->capture();
}

const OIS::MouseState& OGFW::InputSystem::getMouseState()
{
    return mouse->getMouseState();
}

bool OGFW::InputSystem::getKey(const OIS::KeyCode key)
{
    return keyboard->isKeyDown(key);
}

float OGFW::InputSystem::getAxis(OGFW::InputSystem::KeyboardConst axisName)
{
    if (axisName == KEYBOARD_VERTICAL) return *kAXISVertical;
    if (axisName == KEYBOARD_WS) return *kWSVertical;
    if (axisName == KEYBOARD_HORIZONTAL) return *kAXISHorizontal;
    if (axisName == KEYBOARD_AD) return *kADHorizontal;

    return 0;
}