#include "OgreFramework.h"

OGFW::OgreFramework::OgreFramework()
        : mResourcesCfg(Ogre::StringUtil::BLANK),
          mPluginsCfg(Ogre::StringUtil::BLANK),
          mRoot(0),
          mWindow(0),
          mShutdown(false),
          kAXISVertical(0),
          kAXISHorizontal(0),
          kWSVertical(0),
          kADHorizontal(0),
          input(0)
{

}

OGFW::OgreFramework::~OgreFramework()
{
    Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);
    windowClosed(mWindow);
    delete mRoot;
}

void OGFW::OgreFramework::go(void)
{
    mResourcesCfg = "resources.cfg";
    mPluginsCfg = "plugins.cfg";

    if (!setup()) return;

    mRoot->startRendering();
}

bool OGFW::OgreFramework::setup(void)
{
    mRoot = new Ogre::Root(mPluginsCfg);

    setupResources();

    if (!configure()) return false;

    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

    loadResources();
    createScene();
    createFrameListener();

    return true;
}

void OGFW::OgreFramework::setupResources(void)
{
    Ogre::ConfigFile cf;
    cf.load(mResourcesCfg);

    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    Ogre::String secName, typeName, archName;

    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap* settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;

        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;

            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
        }
    }
}

bool OGFW::OgreFramework::configure(void)
{
    if (mRoot->showConfigDialog())
    {
        mWindow = mRoot->initialise(true, "OgreFrame Render Window");

        return true;
    }

    return false;
}

void OGFW::OgreFramework::loadResources(void)
{
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void OGFW::OgreFramework::createFrameListener(void)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");

    input = new InputSystem(mWindow, &kAXISVertical, &kAXISHorizontal, &kWSVertical, &kADHorizontal);

    windowResized(mWindow);

    Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);

    mRoot->addFrameListener(this);
}

bool OGFW::OgreFramework::frameStarted(const Ogre::FrameEvent &evt)
{
    time.deltaTime = evt.timeSinceLastFrame;

    start();

    return true;
}

bool OGFW::OgreFramework::frameRenderingQueued(const Ogre::FrameEvent &evt)
{
    if (mWindow->isClosed()) return false;

    if (mShutdown) return false;

    input->capture();

    time.deltaTime = evt.timeSinceLastFrame;

    kAXISVertical = 0;
    kAXISHorizontal = 0;
    kWSVertical = 0;
    kADHorizontal = 0;

    processAxis();

    update();

    return true;
}

void OGFW::OgreFramework::processAxis()
{
    if (input->getKey(OIS::KC_UP))
    {
        kAXISVertical = -1;
    }
    else if (input->getKey(OIS::KC_W))
    {
        kWSVertical = -1;
    }

    if (input->getKey(OIS::KC_DOWN))
    {
        kAXISVertical = 1;
    }
    else if (input->getKey(OIS::KC_S))
    {
        kWSVertical = 1;
    }

    if (input->getKey(OIS::KC_LEFT))
    {
        kAXISHorizontal = -1;
    }
    else if (input->getKey(OIS::KC_A))
    {
        kADHorizontal = -1;
    }

    if (input->getKey(OIS::KC_RIGHT))
    {
        kAXISHorizontal = 1;
    }
    else if (input->getKey(OIS::KC_D))
    {
        kADHorizontal = 1;
    }
}

bool OGFW::OgreFramework::frameEnded(const Ogre::FrameEvent &evt)
{
    time.deltaTime = evt.timeSinceLastFrame;

    ended();

    return true;
}

void OGFW::OgreFramework::windowResized(Ogre::RenderWindow *rw)
{
    unsigned int width, height, depth;
    int left, top;

    rw->getMetrics(width, height, depth, left, top);

    const OIS::MouseState& ms = input->getMouseState();

    ms.width = width;
    ms.height = height;
}

void OGFW::OgreFramework::windowClosed(Ogre::RenderWindow *rw)
{
    if (rw == mWindow)
    {
        if (input->isRunning())
        {
            delete input;
        }
    }
}