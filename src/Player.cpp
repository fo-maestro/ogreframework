#include "Player.h"

OGFW::Player::Player()
        : rootNode(0),
          rootEntity(0),
          speed(30),
          yawSpeed(0.05f),
          pitchSpeed(0.05f)
{

}

OGFW::Player::~Player() {}

void OGFW::Player::translate(Ogre::Vector3 direction, Ogre::Node::TransformSpace space)
{
    rootNode->translate(direction * speed, space);
}

void OGFW::Player::translate(Ogre::Vector3 direction, Ogre::Node::TransformSpace space, Time time)
{
    rootNode->translate(direction * speed * time.deltaTime, space);
}

void OGFW::Player::rotate(Ogre::Real x, Ogre::Real y)
{
    rootNode->yaw(Ogre::Degree(x * yawSpeed));
    rootNode->pitch(Ogre::Degree(y * pitchSpeed));
}

void OGFW::Player::rotate(Ogre::Real x, Ogre::Real y, Time time)
{
    rootNode->yaw(Ogre::Degree(x * yawSpeed * time.deltaTime));
    rootNode->pitch(Ogre::Degree(y * pitchSpeed * time.deltaTime));
}

void OGFW::Player::setPosition(Ogre::Vector3 position)
{
    rootNode->setPosition(position);
}

Ogre::Vector3 OGFW::Player::getPosition()
{
    return rootNode->getPosition();
}

Ogre::AnimationState* OGFW::Player::getAnimation(Ogre::String animationName)
{
    return rootEntity->getAnimationState(animationName);
}

Ogre::SceneNode* OGFW::Player::getSceneNode()
{
    return rootNode;
}

Ogre::Entity* OGFW::Player::getEntity()
{
    return rootEntity;
}

void OGFW::Player::setSpeed(Ogre::Real speed)
{
    this->speed = speed;
}

void OGFW::Player::setYawSpeed(Ogre::Real speed)
{
    yawSpeed = speed;
}

void OGFW::Player::setPitchSpeed(Ogre::Real speed)
{
    pitchSpeed = speed;
}

Ogre::Real OGFW::Player::getSpeed()
{
    return speed;
}

Ogre::Real OGFW::Player::getYawSpeed()
{
    return yawSpeed;
}

Ogre::Real OGFW::Player::getPitchSpeed()
{
    return pitchSpeed;
}