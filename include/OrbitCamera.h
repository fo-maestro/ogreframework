#ifndef _CAMERAMANAGER_H
#define _CAMERAMANAGER_H

#include <Player.h>

namespace OGFW
{
    void initViewPort(Ogre::RenderWindow* window, Ogre::Camera* camera);

    class OrbitCamera {

    public:
        OrbitCamera(Ogre::SceneManager *sceneManager, Ogre::String cameraName);

        virtual ~OrbitCamera();

        virtual void translate(Ogre::Vector3 direction, Time time);

        virtual void translate(Ogre::Vector3 direction);

        virtual void rotate(Ogre::Real x, Ogre::Real y, Time time);

        virtual void rotate(Ogre::Real x, Ogre::Real y);

        virtual void yaw(Ogre::Degree degree);

        virtual void pitch(Ogre::Degree pitch);

        virtual void setNearClipDistance(Ogre::Real distance);

        virtual Ogre::Real getSpeed();

        virtual Ogre::Real getSensitivyX();

        virtual Ogre::Real getSensitivyY();

        virtual Ogre::Vector3 getCameraPosition();

        virtual Ogre::Quaternion getCameraOrientation();

        virtual Ogre::Vector3 getTargetPosition();

        virtual Ogre::Quaternion getTargetOrientation();

        virtual Ogre::Camera *getCamera();

        virtual Ogre::SceneNode *getTargetNode();

        virtual Ogre::Degree getYaw();

        virtual Ogre::Degree getPitch();

        virtual Ogre::Real getZoomSpeed();

        virtual void setSpeed(Ogre::Real speed);

        virtual void setSensitivy(Ogre::Real sensitivyX, Ogre::Real sensitivyY);

        virtual void setTargetPosition(Ogre::Vector3 position);

        virtual void setTargetOrientation(Ogre::Quaternion orientation);

        virtual void setCameraElevation(Ogre::Real elevation);

        virtual void setCameraDistance(Ogre::Real distance);

        virtual void setZoomSpeed(Ogre::Real speed);

        virtual void zoom(Ogre::Real zoom);

        virtual void zoom(Ogre::Real zoom, Time time);

    private:
        Ogre::Real speed;
        Ogre::Real sensitivyX;
        Ogre::Real sensitivyY;
        Ogre::Real zoomSpeed;

        Ogre::Camera *cam;
        Ogre::SceneNode *cam_node;
        Ogre::SceneNode* pitch_node;
        Ogre::Degree mYaw;
        Ogre::Degree mPitch;
    };
}

#endif
