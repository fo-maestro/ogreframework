#ifndef _PLAYER_H
#define _PLAYER_H

#include <Ogre.h>
#include <FrameTime.h>

namespace OGFW
{
    class Player
    {
    public:
        Player();
        virtual ~Player();
        virtual void createPlayer() = 0;
        virtual void translate(Ogre::Vector3 direction, Ogre::Node::TransformSpace space);
        virtual void translate(Ogre::Vector3 direction, Ogre::Node::TransformSpace space, Time time);
        virtual void rotate(Ogre::Real x, Ogre::Real y);
        virtual void rotate(Ogre::Real x, Ogre::Real y, Time time);
        virtual void setPosition(Ogre::Vector3 position);
        virtual void setSpeed(Ogre::Real speed);
        virtual void setYawSpeed(Ogre::Real speed);
        virtual void setPitchSpeed(Ogre::Real speed);
        virtual Ogre::Vector3 getPosition();
        virtual Ogre::AnimationState* getAnimation(Ogre::String animationName);
        virtual Ogre::SceneNode* getSceneNode();
        virtual Ogre::Entity* getEntity();
        virtual Ogre::Real getSpeed();
        virtual Ogre::Real getYawSpeed();
        virtual Ogre::Real getPitchSpeed();

    protected:
        Ogre::SceneNode* rootNode;
        Ogre::Entity* rootEntity;
        Ogre::Real speed;
        Ogre::Real yawSpeed;
        Ogre::Real pitchSpeed;
    };
}

#endif
