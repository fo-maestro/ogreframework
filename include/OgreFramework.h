#ifndef _OGREFRAMEWORK_H
#define _OGREFRAMEWORK_H

#include <Ogre.h>
#include <OIS.h>
#include <FrameTime.h>
#include <InputSystem.h>
#include <OrbitCamera.h>

#ifdef OGRE_STATIC_LIB
#   define OGRE_STATIC_GL
#   if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#       define OGRE_STATIC_Direct3D9
#       if OGRE_USE_D3D10
#           define OGRE_STATIC_Direct3D10
#       endif
#   endif
#   define OGRE_STATIC_BSPSceneMAnager
#   define OGRE_STATIC_ParticleFX
#   define OGRE_STATIC_CgProgramManager
#   ifdef OGRE_USE_PCZ
#       define OGRE_STATIC_PCZSceneManager
#       define OGRE_STATIC_OctreeZone
#   else
#       define OGRE_STATIC_OctreeSceneManager
#   endif
#   include "OgreStaticPluginLoader.h"
#endif

namespace OGFW {

    class OgreFramework
            : public Ogre::WindowEventListener,
              public Ogre::FrameListener {
    public:
        OgreFramework();

        virtual ~OgreFramework();

        virtual void go(void);

    private:
        bool frameStarted(const Ogre::FrameEvent &evt);

        bool frameRenderingQueued(const Ogre::FrameEvent &evt);

        bool frameEnded(const Ogre::FrameEvent &evt);

        void processAxis();

        float kAXISVertical;
        float kAXISHorizontal;
        float kWSVertical;
        float kADHorizontal;

    protected:
        virtual bool setup(void);

        virtual void setupResources(void);

        virtual bool configure(void);

        virtual void loadResources(void);

        virtual void createScene(void) = 0;

        virtual void createFrameListener(void);

        virtual void start() = 0;

        virtual void update() = 0;

        virtual void ended() = 0;

        virtual void windowResized(Ogre::RenderWindow *rw);

        virtual void windowClosed(Ogre::RenderWindow *rw);

        Ogre::String mResourcesCfg;
        Ogre::String mPluginsCfg;
        Ogre::Root *mRoot;
        Ogre::RenderWindow *mWindow;

        bool mShutdown;

        InputSystem *input;
        Time time;
    };
}

#endif //_OGREFRAMEWORK_H
