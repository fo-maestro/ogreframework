#ifndef _FRAMETIME_H
#define _FRAMETIME_H

namespace OGFW
{
    class Time {
    public:
        Ogre::Real deltaTime;
    };
}

#endif //_FRAMETIME_H
