#ifndef _INPUT_H
#define _INPUT_H

namespace OGFW
{
    class InputSystem {
    public:
        InputSystem(Ogre::RenderWindow* window,
              float *kAXISVertical,
              float *kAXISHorizontal,
              float *kWSVertical,
              float *kADHorizontal);

        ~InputSystem();

        enum KeyboardConst
        {
            KEYBOARD_VERTICAL,
            KEYBOARD_HORIZONTAL,
            KEYBOARD_AD,
            KEYBOARD_WS
        };

        bool getKey(const OIS::KeyCode key);

        float getAxis(OGFW::InputSystem::KeyboardConst axisName);

        bool isRunning();

        void setKeyListener(OIS::KeyListener* listener);

        void setMouseListener(OIS::MouseListener* listener);

        void capture();

        const OIS::MouseState& getMouseState();

    private:
        OIS::InputManager *inputManager;
        OIS::Keyboard *keyboard;
        OIS::Mouse *mouse;
        float *kAXISVertical;
        float *kAXISHorizontal;
        float *kWSVertical;
        float *kADHorizontal;
    };
}

#endif //_INPUT_H
