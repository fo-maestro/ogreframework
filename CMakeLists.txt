cmake_minimum_required(VERSION 3.0)
project(OgreFramework)

if(WIN32)
    set(CMAKE_MODULE_PATH "$ENV{OGRE_HOME}/CMake/;${CMAKE_MODULE_PATH}")
endif(WIN32)

if(UNIX)
    if(EXISTS "/usr/local/lib/OGRE/cmake")
        set(CMAKE_MODULE_PATH "/usr/local/lib/OGRE/cmake;${CMAKE_MODULE_PATH}")
    elseif(EXISTS "/usr/lib/OGRE/cmake")
        set(CMAKE_MODULE_PATH "/usr/lib/OGRE/cmake;${CMAKE_MODULE_PATH}")
    elseif(EXISTS "/usr/share/OGRE/cmake/modules")
        set(CMAKE_MODULE_PATH  "/usr/share/OGRE/cmake/modules;${CMAKE_MODULE_PATH}")
    else()
        message(SEND_ERROR "Failed to find module path.")
    endif(EXISTS "/usr/local/lib/OGRE/cmake")
endif(UNIX)

find_package(OGRE REQUIRED)

find_package(OIS REQUIRED)

if(NOT OIS_FOUND)
    message(SEND_ERROR "Failed to find OIS.")
endif()

if(NOT OGRE_BUILD_PLATFORM_IPHONE)
    if(WIN32 OR APPLE)
        set(Boost_USE_STATIC_LIBS TRUE)
    else()
        set(Boost_USE_STATIC_LIBS ${OGRE_STATIC})
    endif()

    if(MINGW)
        set(CMAKE_FIND_LIBRARY_PREFIXES ${CMAKE_FIND_LIBRARY_PREFIXES} "")
    endif ()
    set(OGRE_BOOST_COMPONENTS thread date_time)
    find_package(Boost COMPONENTS ${OGRE_BOOST_COMPONENTS} QUIET)

    if(NOT Boost_FOUND)
        set(Boots_USE_STATIC_LIBS NOT ${Boost_USE_STATIC_LIBS})
        find_package(Boost COMPONENTS ${OGRE_BOOST_COMPONENTS} QUIET)
    endif()

    find_package(Boost QUIET)
endif()

include_directories(${OGRE_INCLUDE_DIR} ${OIS_INCLUDE_DIR} ${OGRE_Overlay_INCLUDE_DIR} include)

file(GLOB SOURCE_FILES "src/*.cpp")

file(GLOB HEADER_FILES "include/*.h")

if(WIN32)
    file(COPY plugins/win32/plugins.cfg DESTINATION ${CMAKE_SOURCE_DIR})
elseif(UNIX)
    file(COPY plugins/unix/plugins.cfg DESTINATION ${CMAKE_SOURCE_DIR})
endif()

add_library(OgreFramework SHARED ${HEADER_FILES} ${SOURCE_FILES})

target_link_libraries(OgreFramework ${OGRE_LIBRARIES} ${OIS_LIBRARIES} ${OGRE_Paging_LIBRARIES} ${OGRE_Terrain_LIBRARIES} ${OGRE_Property_LIBRARIES} ${OGRE_RTShaderSystem_LIBRARIES} ${OGRE_Volume_LIBRARIES} ${OGRE_Overlay_LIBRARIES})
