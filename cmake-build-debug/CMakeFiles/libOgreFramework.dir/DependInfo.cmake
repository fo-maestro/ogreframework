# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/felipe/ClionProjects/OgreFramework/src/CameraManager.cpp" "/home/felipe/ClionProjects/OgreFramework/cmake-build-debug/CMakeFiles/libOgreFramework.dir/src/CameraManager.cpp.o"
  "/home/felipe/ClionProjects/OgreFramework/src/Input.cpp" "/home/felipe/ClionProjects/OgreFramework/cmake-build-debug/CMakeFiles/libOgreFramework.dir/src/Input.cpp.o"
  "/home/felipe/ClionProjects/OgreFramework/src/OgreFramework.cpp" "/home/felipe/ClionProjects/OgreFramework/cmake-build-debug/CMakeFiles/libOgreFramework.dir/src/OgreFramework.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/OGRE"
  "/usr/include/ois"
  "/usr/include/OGRE/Overlay"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
