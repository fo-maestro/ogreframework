# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/felipe/ClionProjects/OgreFramework/src/InputSystem.cpp" "/home/felipe/ClionProjects/OgreFramework/cmake-build-debug/CMakeFiles/OgreFramework.dir/src/InputSystem.cpp.o"
  "/home/felipe/ClionProjects/OgreFramework/src/OgreFramework.cpp" "/home/felipe/ClionProjects/OgreFramework/cmake-build-debug/CMakeFiles/OgreFramework.dir/src/OgreFramework.cpp.o"
  "/home/felipe/ClionProjects/OgreFramework/src/OrbitCamera.cpp" "/home/felipe/ClionProjects/OgreFramework/cmake-build-debug/CMakeFiles/OgreFramework.dir/src/OrbitCamera.cpp.o"
  "/home/felipe/ClionProjects/OgreFramework/src/Player.cpp" "/home/felipe/ClionProjects/OgreFramework/cmake-build-debug/CMakeFiles/OgreFramework.dir/src/Player.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/OGRE"
  "/usr/include/ois"
  "/usr/include/OGRE/Overlay"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
